#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>

#define TL 600
#define TT 10000

int ladrillosAlmacen = 0;
bool estadoFabrica = false;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void addLadrillos() {
    pthread_mutex_lock(&mutex);
    ladrillosAlmacen++;
    pthread_mutex_unlock(&mutex);
}

int getLadrillos(int ladrillos, pthread_t thread_id) {
    pthread_mutex_lock(&mutex);
    if(ladrillosAlmacen > 0) {
        if (ladrillosAlmacen >= ladrillos) {
            ladrillosAlmacen -= ladrillos;
            printf("| HILO: %li | HA COGIDO %i LADRILLOS.\n", thread_id, ladrillos);
        } else {
            printf("| HILO: %li | NO HAY SUFICIENTES LADRILLOS.\n", thread_id);
            ladrillos = 0;
        }
    } else {
        printf("| HILO: %li | NO HAY LADRILLOS EN LA FABRICA.\n", thread_id);
        ladrillos = 0;
        if (estadoFabrica == false) {
            ladrillos = -1;
            printf("|| HILO: %li || LA FABRICA ESTA CERRADA.\n", thread_id);
        }
    }
    pthread_mutex_unlock(&mutex);
    return ladrillos;
}
void* fabrica (void*) {
    estadoFabrica = true;
    for (int i=0; i<TL; i++) {
        usleep(TT);
        addLadrillos();
        printf("||||| LA FABRICA HA PRODUCIDO %i LADRILLO |||||\n", i);
    }
    estadoFabrica = false;
}

void* obrero (void*) {
    pthread_t threadObrero_id = pthread_self();
    int ladrillos, sacados, totalSacados = 0;
    while (true) {
        usleep(TT);
        ladrillos = 1 + rand()%2;
        sacados = getLadrillos(ladrillos, threadObrero_id);
        if (sacados < 0)
            pthread_exit(NULL);
        totalSacados += sacados;
        printf("| HILO: %li | LLEVA SACADOS %i LADRILLOS DE LA FABRICA.\n", threadObrero_id, totalSacados);
        usleep(ladrillos*TT);
    }
}

int main () {
    pthread_t threadFabrica_id, threadLuis_id, threadPepe_id;


    printf("||||| LA FABRICA SE PONE A PRODUCIR LADRILLOS |||||\n");
    pthread_create(&threadFabrica_id, NULL, &fabrica, NULL);

    while (estadoFabrica == false) {
        usleep(TT);
    }

    printf("||||| LUIS SE PONE A TRABAJAR |||||\n");
    pthread_create(&threadLuis_id, NULL, &obrero, NULL);
    printf("||||| PEPE SE PONE A TRABAJAR |||||\n");
    pthread_create(&threadPepe_id, NULL, &obrero, NULL);

    pthread_join(threadFabrica_id, NULL);
    pthread_join(threadLuis_id, NULL);
    pthread_join(threadPepe_id, NULL);
    return 0;
}
