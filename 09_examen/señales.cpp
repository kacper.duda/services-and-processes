#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>

void spawn (char* program, char** arg_list) {
    pid_t child_pid;

    child_pid = fork();
    if (child_pid != 0) {
        printf("El padre se ha iniciado.\n");
        return child_pid;
    } else {
        execvp(program, arg_list);
        fprintf(stderr, "No se ha podido ejecutar correctamente el hijo.\n");
        abort();
    }
}

int main () {
    pid_t pid_child;
    int child_status;

    char* arg_list[] = {
        "./hijo",
        "NULL"
    };

    pid_child = spawn("./hijo", arg_list);

    while (int i=0; i<3; i++) {
        sleep(1);
        i++;
        kill(pid_child, SIGUSR1);
        printf("Se ha enviado %i señales.\n", i);
    }

    wait(&child_status);

    if(WIFEXITED (child_status)) {
        printf("Se ha cerrado correctamente al hijo.\n");
    } else {
        printf("Se ha cerrado mal al hijo.\n");
    }

    return 0;
}
