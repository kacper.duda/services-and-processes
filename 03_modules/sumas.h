#ifndef __SUMAS_H__
#define __SUMAS_H__
#ifdef __cplusplus
extern "C" {
#endif
    const char **catalogo();
    int add (int op1, int op2);
    int sub (int op1, int op2);
#ifdef __cplusplus
}
#endif
#endif
