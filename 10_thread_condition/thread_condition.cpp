#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

int thread_flag;
int i = 0;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;

void initialize_flag() {
    /* INICIALIZA EL MUTEX Y LA VARIABLE DE CONCION. */
    pthread_mutex_init (&thread_flag_mutex, NULL);
    pthread_cond_init (&thread_flag_cv, NULL);
    /* INICIALIZA LA VARIBLE "thread_flag". */
    thread_flag = 0;
}

void do_work () {
    sleep(1);
    i += 5;
    fflush(stdout);
    printf(" Contador: %i\r",i);
}

/* REALIZA UNA LLAMADA A LA FUNCION "do_work" MIENTRAS QUE LA VARIABLE "thread_flag" ES ESTABLECIDA.
 * BLOQUEA SI LA FLAG ESTA VACIA. */
void* thread_function (void* thread_arg) {
    /* ENTRA EN UN BUCLE INFINITO. */
    while (1) {
        /* BLOQUEA EL MUTEX ANTES DE ACCEDER A LA FLAG. */
        pthread_mutex_lock (&thread_flag_mutex);
        while (!thread_flag) {
            /* LA FLAG ESTA VACIA.
             * ESPERA UNA SEÑAL EN LA VARIABLE DE CONDICION, INDICANDO QUE EL VALOR DE LA FLAG HA CAMBIADO.
             * CUANDO LA SEÑAL LLEGA Y EL THREAD SE DESBLOQUEA, ENTRA EN BUCLE Y COMPRUEBA LA FLAG OTRA VEZ. */
            pthread_cond_wait (&thread_flag_cv, &thread_flag_mutex);
        }
        /* CUANDO LLEGA HASTA AQUI, YA SABE LA FLAG QUE TIENE QUE SER ESTABLECIDA.
         * DESBLOQUEA EL MUTEX. */
        pthread_mutex_unlock (&thread_flag_mutex);
        /* EJECUTA LA FUNCION "do_work()." */
        do_work();
    }
    return NULL;
}

/*ESTABLECE EL VALOR DE LA VARIABLE "thread_flag" A "FLAG_VALUE". */
void set_thread_flag (int flag_value) {
    /* BLOQUEA EL MUTEX ANTES DE ACCEDER AL VALOR DE LA FLAG. */
    pthread_mutex_lock (&thread_flag_mutex);
    /* ESTABLECE EL VALOR DE LA FLAG Y LA SEÑAL EN EL CASO DE QUE "thread_function" SEA BLOQUEADO,
     * SE QUEDA ESPERANDO PARA QUE EL FLAG SE CAMBIE. SIN EMBARGO, "thread_function" NO PUEDE
     * COMPROBAR LA FLAG HASTA QUE EL MUTEX SEA DESBLOQUEADO */
    thread_flag = flag_value;
    pthread_cond_signal (&thread_flag_cv);
    /* DESBLOQUEA EL MUTEX */
    pthread_mutex_unlock (&thread_flag_mutex);
}

int main () {
    pthread_t thread_id;

    initialize_flag();
    set_thread_flag(1);

    pthread_create(&thread_id, NULL, &thread_function, NULL);
    pthread_join(thread_id, NULL);

    return 0;
}
