#include <pthread.h>
#include <stdio.h>

// Parametros para la función imprimir
struct char_print_parms
{
    char character; // Caracter a imprimir
    int count; // Numero de veces que se va a imprimir
};

// Imprime un numero de caracteres al tubo "stderr", con el nombre "PARAMETERS", el cual es un puntero a la estructura "char_print_parms"
void* char_print (void* parameters)
{
    // Aplica un molde al puntero al tipo correcto
    struct char_print_parms* p = (struct char_print_parms*) parameters;
    int i;

    for (i = 0; i < p -> count; ++i)
        fputc (p -> character, stderr);
    return NULL;
}

// Programa principal
int main () {
    pthread_t thread1_id;
    pthread_t thread2_id;
    struct char_print_parms thread1_args;
    struct char_print_parms thread2_args;

    // Crea un nuevo hilo para imprimir 30,000 caracteres 'x'
    thread1_args.character = 'x';
    thread1_args.count = 30000;
    pthread_create (&thread1_id, NULL, &char_print, &thread1_args);

    // Crea un nuevo hilo para imprimir 20,000 caracteres 'o'
    thread2_args.character = 'o';
    thread2_args.count = 20000;
    pthread_create (&thread2_id, NULL, &char_print, &thread2_args);

    // Hasta que no finalicen los hilos no se cierra
    pthread_join (thread1_id, NULL);
    pthread_join (thread2_id, NULL);

    return 0;
}
