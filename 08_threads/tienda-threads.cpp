#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

struct char_print_parms {
    char character;
    int count;
};

void* char_print (void* parameters) {
    struct char_print_parms* p = (struct char_print_parms*) parameters;
    int i;

    for (i = 0; i < p->count; i++)
        fputc (p->character, stderr);
    return NULL;
}

int main (int argc, char *argv[]) {
    int nHilo = 1;
    int opcion;

    pthread_t thread_id;

    struct char_print_parms thread_args;

    system("clear");

    do {
        printf("\n");
        system("toilet -fpagga TIENDA");
        printf("\n1. Crear un hilo.");
        printf("\n2. Borrar un hilo.");
        printf("\n3. Cerrar tienda.\n\n\n");

        printf("Selecciona una de las opciones (1,2,3): ");
        scanf("%i", &opcion);

        switch (opcion) {
            case 1:
                system("clear");
                printf("Has seleccionado la opcion:\n\t1. Crear un hilo.\n\n");
                thread_args.character = nHilo;
                thread_args.count = 1;
                printf("Thread numero: ");
                pthread_create (&thread_id, NULL, &char_print, &thread_args);
                pthread_join(thread_id, NULL);
                printf("\n\n\n");
                nHilo++;
                break;
            case 2:
                system("clear");
                printf("Has seleccionado la opcion:\n\t2. Borrar un hilo.\n\n");
                break;
            case 3:
                system("clear");
                printf("Has seleccionado la opcion:\n\t3. Cerrar tienda.\n\n");
                printf("Los hilos estan terminando.\n\n");
                break;
            default:
                printf("La opcion que has seleccionado no existe, seleccione otra.\n\n");
                break;
        }
    } while (opcion != 3);

    return EXIT_SUCCESS;
}
