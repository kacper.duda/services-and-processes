#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int spawn(char* program, char** arg_list) {
    // pid_t es un alias de int
    pid_t child_pid;

    // fork divide el proceso en 2 (con el mismo contenido del padre)
    child_pid = fork();
    // comprueba si al padre le ha devuelto un valor
    if (child_pid != 0) {
      return child_pid;
    // en el caso que devuelva otro valor de 0 se ejecuta el hijo
    } else {
      // ejecuta el programa del hijo
      execvp(program, arg_list);
      fprintf(stderr, "Error al ejecutar el programa\n");
      abort();
    }
}

int main() {

    int child_status;
    char* arg_list[] = {
        "./suma",
        "2",
        "4",
        NULL
    };
    spawn("./suma", arg_list);

    wait(&child_status);
    if (WIFEXITED (child_status))
        printf("El programa hijo ha terminado correctamente, con el exit code %d\n", WEXITSTATUS (child_status));
    else
        printf("El programa hijo ha terminado con errores.\n");

    printf("El programa ha acabado la ejecucion.\n");

    return 0;
}
